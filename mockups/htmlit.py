# This is my adhoc build script because I'm to lazy to relearn make
# and i'm bad at shell scripting, and sed is confusing to me
import os
import subprocess
OUT_PATH = os.path.realpath('.')
HAML_PATH = '{0}/{1}'.format(OUT_PATH, 'haml')
SKIP = ['.swp']
def skip(f):
    for s in SKIP:
        if f.endswith(s):
            return True
    return False

for (path, dirs, files) in os.walk(HAML_PATH):
    for f in files:
        if skip(f):
            continue
        i = '{0}/{1}'.format(path,f)
        o = '{0}/{1}.html'.format(OUT_PATH, f.split('.')[0])
        convert = subprocess.Popen(
            ['haml', '-f', 'html5', '-e', '--trace', i, o]
        )
