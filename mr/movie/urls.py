from django.conf.urls import patterns, url

from . import views as movie_views

urlpatterns = patterns('',
  url(r'^$', movie_views.Movie.as_view(), name='movie-home'),
)