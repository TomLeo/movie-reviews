from tastypie.utils.timezone import now
from tastypie.resources import ModelResource
from django.db import models

class movie(models.Model):
    title = models.CharField(max_length=255)
    #actors = m2m
    #directory = foreign_key
    runtime = models.PositiveIntegerField()

    def __unicode__(self):
        return self.title