# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'movie_review'
        db.create_table(u'review_movie_review', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('review', self.gf('django.db.models.fields.TextField')()),
            ('rating', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'review', ['movie_review'])


    def backwards(self, orm):
        # Deleting model 'movie_review'
        db.delete_table(u'review_movie_review')


    models = {
        u'review.movie_review': {
            'Meta': {'object_name': 'movie_review'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rating': ('django.db.models.fields.IntegerField', [], {}),
            'review': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['review']