Chicken Stratches
================

The following is what I like to call chicken stratches it is a rampling
of ideas that probably will only make sence to me. This file will also
be filled with grammer and spelling mistakes. So if you have questions
just read the code and code comments.

off that,
Tom

TODO
=====

- Look at old concepts
- Learn an API
   doc: http://docs.themoviedb.apiary.io/
   key: 6fc9e11b1c8259cccd5afeaf4c225bf0
- Icons
  http://www.tenbytwenty.com/sosa.php

- Include mockups/img/back_drop.jpg

The Development Process
=======================

designing and layout
--------------------

designing and layout will be done in the /mockups folder
mockups will be written in the /mockups/haml folder and when
a desired design is ready for impelementation the html output
will be used to create the django templates as apposed to 
something like https://github.com/jessemiller/HamlPy.git

the reason I won't compile strait to django templates from haml is because
haml was not designed to compile into django templates and although
you can create templates this way, it would ignore the abstraction
and includes that will come latter in the development process

CSS Design Patterns
http://bradfrost.github.com/this-is-responsive/patterns.html

django config
-------------

Reference for all the things
   https://github.com/nathanborror/django-basic-apps/tree/master/basic

Template Debugging Addon
   https://github.com/django-debug-toolbar/django-debug-toolbar#readme

Registration
   https://bitbucket.org/ubernostrum/django-registration/
   Copy from this as much as possible, but write own

Profiles
   https://bitbucket.org/ubernostrum/django-profiles/
   Copy from this as much as possible, but write own

Searching
   Haystack+Elasticsearch

Move Ratings
============

Name/Branding Ideas
-------------------

Allow people to rate movies, and use the RESTfull API to access their ratings
and include them onto their own websites/applications.

Rating movies should leverage some API like IMDB to populate movie fields,
if the movie is not found then the user can fill out the information themselves

Permissions
-----------

Reviews can be limited to only clan members, or they can be available to the
public. 

User
----
- email
- alias (what people see)

Clan
----
- group of users who watch and view movies togeather
- could be similar to a meetup where you can organize members to meetup and
  watch movies togeather. Save places in line for movie premeirs. Share DVD's
  with eachother. Threaded messaging restricted to just clan members.
- users can be members of multiple clans
- should allow people with podcast or editorials to set up their idenity on the
  platform

content_blob
------------

-user or clan

content_blob_video
------------------

-link to video [youtube, vimeo, mp3 file, html5 video]

content_blob_audio
------------------

-link to audio [mp3, youtube, soundcloud]

content_blog_text
-----------------

-support rst/md/html/plain-text

content_blog_rating
-------------------

1-5, however the user can define what their scale means example allowing a bit
of personalitly to each rating.

1 = Crap
2 = Seriously Bad
3 = Go rend this movie
4 = Maybe see this movie
5 = See this movie ASAP

1 = A giant pile of pooo
5 = I love this movie


Batch Review
------------

-reviews
-clan or user

- optional: allow for video reviews/content
- optional: auto reviews/podcasts
- optional: text review
- optional: 1-5 star rating

Should batch reviews to limmited to reviews "owned" by the user or clan, or
could a user/clan put togeather other peoples reviews than give their insite on
those reviews?

represent multiple movies being reviewed at one time by a user or a group. This
might apply to a podcast that reviews a few different movies a week. This could
apply to someone who is creating a top 10 list of the movies they saw that year.

Review
------

-user or clan
-movie (link to movie)
-recommend this movie (y/n)

- optional: allow for video reviews/content
- optional: auto reviews/podcasts
- optional: text review
- optional: 1-5 star rating

Reviews should be generic, meaning they can apply to more than one movie.
Reviews can be on behalf of a person or a clan

A reviewer might not want to give a movie a specific star, but istead express
their opinion in the form of a well articulated review. Some people might
not give a shit about taking the time to review a movie (we all have busy
lives). Some reviwers might prefer to have video reviews of a movie. A group of
people might like to get togeather and talk about a bunch of movies they just
saw.


RESTful
=======

/account                register/login
/account/id/            view profile
/account/id/edit        edit profile

/movie/review           review form
/movie/review/slug      slug of group or user
/movie/review/slug/1    redirects to slug if 1
/movie/review/slug/2    for more than one reivew by the same usr/group



