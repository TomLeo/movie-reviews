import ipdb
import json
import requests
from PIL import Image
from StringIO import StringIO

class allthethings(object):
    MURL = 'http://api.themoviedb.org/3/'
    #MURL = 'http://themoviedb.apiary.io/3/'
    API_KEY = '6fc9e11b1c8259cccd5afeaf4c225bf0'
    
    js_header = {'content-type': 'application/json'}

    api_key_params = {'api_key': API_KEY}
    
    def get_url(self, url):
        return '{0}{1}'.format(self.MURL, url)

if __name__ == '__main__':
    t = allthethings()

    url = t.get_url('search/movie')
    params = {'query': 'iron man'}
    params.update(t.api_key_params)

    data = requests.get(url, params=params).json()['results'][0]
    print 'Title: ', data['title']
    print 'Release: ', data['release_date']
    print 'Rating: ', data['vote_average']

    print 'Meta:'
    movie_id = data['id']
    print 'Id: ', movie_id
    print 'Poster Path', data['poster_path']
    print 'Backdrop Path', data['backdrop_path']
    
    images_url = t.get_url('movie/{id}/images'.format(id=movie_id))
    params = t.api_key_params
    backdrop_image_path = requests.get(images_url, params=params).json()['backdrops'][0]['file_path']
    #backdrop_image_path = '{0}/backdrops{1}'.format(images_url, backdrop_image)

    base_url = t.get_url('configuration')
    params = t.api_key_params
    qual_request = requests.get(base_url, params=params).json()

    #qual_request = requests.get(backdrop_image_path, params=params).json()['images']

    image_qual_url = '{0}{1}{2}'.format(qual_request['images']['base_url'],
                                        qual_request['images']['backdrop_sizes'][2],
                                        backdrop_image_path)

    image_bin_request = requests.get(image_qual_url)
    image_bin = Image.open(StringIO(image_bin_request.content))
    image_bin.save('/home/tom/movie-reviews/test.jpg', format='JPEG')

    ipdb.set_trace()

    #image_bin = Image.open(StringIO(content))
    #i.save(format='JPEG')
